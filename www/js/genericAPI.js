angular.module('starter').service("genericAPI", function ($http) {


	function _auth (data, scope) {
        
    	return $http({
            method: data.request,
            url: "http://apidweb.solverp.com.br/src/rest/"+data.class+".php",
            headers : {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'},
            transformRequest: function(obj) {
		       	var str = [];
		        for(var p in obj)
		        // str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
		    	
			    	if ( typeof( obj[p] ) === 'object' ) {
			    		str.push(  p + "=" + JSON.stringify( obj[p] ) );
			    	}else{
			    		str.push( p + "=" + obj[p] );
			    	}
		    	return str.join("&");
		    },
            data: {
	    		metodo : data.metodo,
	    		usuario: data.data.usuario,
	    		senha: 	data.data.senha
	    	}
        });
    };

    function _generic (data, scope) {
        
    	return $http({
            method: data.request,
            url: "http://apidweb.solverp.com.br/src/rest/"+data.class+".php",
            headers : {'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'},
            transformRequest: function(obj) {
		    	var str = [];
		        for(var p in obj)
			       	if ( typeof( obj[p] ) === 'object' ) {
			    		str.push(  p + "=" + JSON.stringify( obj[p] ) );
			    	}else{
			    		str.push( p + "=" + obj[p] );
			    	}
		        return str.join("&");
		    },
            data: {
	    		metodo : data.metodo,
	    		usuario: JSON.parse( localStorage.usuario ),
	    		data: data.data
	    	}
        });
    };

    return {
    	auth: _auth,
        generic: _generic
    };
});