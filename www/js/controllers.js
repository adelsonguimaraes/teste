angular.module('starter.controllers', [])

.controller('loginCtrl', function($rootScope, $scope, genericAPI, $ionicPopup, $location) {
  $rootScope.usuario = '';

  if ( localStorage.usuario ) {
    $location.path('/banco');
    return false;
  }

  $scope.acessar = function ( obj ) {
    obj.senha = MD5(obj.senha);

    var dados = {
      "request" : "POST",
      "class"   : "authentication",
      "metodo"  : "logar",
      "data"    : obj
    }

    // An alert dialog
    $scope.showAlert = function( msg) {
      var alertPopup = $ionicPopup.alert({
        title: 'Atenção',
        template: msg
      });

      alertPopup.then(function(res) {
        // console.log('Thank you for not eating my delicious ice cream cone');
      });
    };

    genericAPI.auth(dados)
      .then(function successCallback(response) {
          if ( response.data.success === true ) {
            // $scope.showAlert( "Seja bem vindo " + response.data.data.usuario );
            localStorage.usuario = JSON.stringify( response.data.data );
            $location.path('/tab/dash');
          }else{
            $scope.showAlert( response.data.msg );
          }
      }, function errorCallback(response) {
      });

  }
})

.controller('bancoCtrl', function( $scope, genericAPI, $ionicPopup, $http ) {
    
  // An alert dialog
  $scope.showAlert = function( msg) {
    var alertPopup = $ionicPopup.alert({
      title: 'Atenção',
      template: msg
    });

    alertPopup.then(function(res) {
      // console.log('Thank you for not eating my delicious ice cream cone');
    });
  };


  function listar () {
   $http({
        method: 'GET',
        url: "http://apidweb.solverp.com.br/src/rest/banco.php?metodo=listarTudo",
    }).then(function successCallback(response) {
        if ( response.data.success === true ) {
          $scope.bancos = response.data.data;
        }
    }, function errorCallback(response) {
    });
  }
  listar();

  $scope.cadastrar = function ( obj ) {

    var dados = {
      "request" : "POST",
      "class"   : "banco",
      "metodo"  : "cadastrar",
      "data"    : obj
    }

    genericAPI.generic(dados)
    .then(function successCallback(response) {
        if ( response.data.success === true ) {
          $scope.showAlert( "Banco castrado com successo!" );
          listar();
        }else{
          $scope.showAlert( response.data.msg );
        }
    }, function errorCallback(response) {
    });
  }

})

.controller('DashCtrl', function($scope) {})

.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };

  // alert
  $scope.alert = function(chat) {
    console.log( chat );
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
});
